﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SimpleWindow
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Create file picker
            using (var form = new OpenFileDialog())
            {
                // Set attributes to only take pdf files
                form.Filter = "pdf files (*.pdf)|*.pdf";
                form.CheckFileExists = true;
                form.CheckPathExists = true;
                form.Multiselect = false;

                if (form.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        // Remove last document and load new pdf file
                        pdfViewer1.Document?.Dispose();
                        pdfViewer1.Document = PdfiumViewer.PdfDocument.Load(form.FileName);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(this, ex.Message, "Open ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
